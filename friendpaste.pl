#!/usr/bin/perl

###
### Perl script to paste things to Friendpaste
###
### E.g., $ friendpaste.pl blah.c 
###       $ cat blah.c | friendpaste.pl
###
### Requires: Perl, LWP, Crypt::SSLeay
###
### kian.mohageri@gmail.com 2009
###

use strict;
use warnings;

use JSON ();
use LWP::UserAgent ();

if ($#ARGV > 0) {
	print "Usage: $0 filename\n";
	exit 1;
}

my $filename = '';
my @snippet;

if (!defined($ARGV[0])) {
	@snippet = <STDIN>;
} else {
	$filename = $ARGV[0];
	open(FH, $filename) or die "Cannot open file: $!";
	@snippet = <FH>;
	close(FH);
}

my $doc = { 
    'title'    => $filename,
    'snippet'  => join('', @snippet),
    'language' => 'text', 
};

my $response = LWP::UserAgent->new->post(
    'https://www.friendpaste.com/', 
    'Content-Type' => 'application/json',
    'Accept'       => 'application/json',
    'Connection'   => 'close', 
    'Content'      => JSON::encode_json($doc),
);

if ($response->is_success) {
	my $json = JSON::decode_json($response->content);

	if ($json->{'ok'} eq 'true') {
		print $json->{'url'} . "\n";
	} else {
		print "Error: " . $json->{'reason'} . "\n";
	}
} else {
	print "Error: " . $response->status_line . "\n";
}
