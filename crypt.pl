#!/usr/bin/perl
# source: https://gist.github.com/Fleshgrinder/3349662
use strict;

chomp(my $filename=$ARGV[0]);
chomp(my $username=$ARGV[1]);
chomp(my $password=$ARGV[2]);

if (!$filename || !$username || !$password) {
  print "USAGE: ./crypt.pl filename username password\n\n";
} else {
  open FILE, ">>", $filename or die $!;
  print FILE $username . ":" . crypt($password, $username) . "\n";
  close FILE or die $!;
}
